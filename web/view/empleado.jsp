<%-- 
    Document   : empleado
    Created on : 16/03/2016, 08:07:52 PM
    Author     : ALUMNO
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
//Capturar los valores enviados a esta pagina
String x=null;
if(request.getParameter("x")!=null){
   x=request.getParameter("x");
   //out.println("opcion x "+x);
}    
       
//habilitar el uso de variables de sesion
    HttpSession s=request.getSession();
    //rescatar los datos guardados en las variables de sesion
    ResultSet rs=null;    
	ResultSet rsc=null;
	if(s.getAttribute("scarga")!=null){
    rsc=(ResultSet)s.getAttribute("scarga");
	rsc.next(); //posicionar en la primera fila
    s.removeAttribute("scarga");
    }
    if(s.getAttribute("sdata")!=null){
    rs=(ResultSet)s.getAttribute("sdata");
    s.removeAttribute("sdata");
    }
    
    String error=null;    
    if(s.getAttribute("serror")!=null){
    error=(String)s.getAttribute("serror");
    s.removeAttribute("serror");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Empleados</title>
    </head>
    <script language="javascript">
	function insertar(){
		location.href="empleado.jsp?x=Insertar";
	}
	function buscar(){
		location.href="empleado.jsp?x=Buscar";
	}
        function listado(){
            location.href="/SistemaWebJSP/EmpleadoController?op=listado";
        }
        </script>
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
<body>
        <h1>Empleados</h1>
        <br>
<form id="form1" name="form1" method="post" action="/SistemaWebJSP/EmpleadoController">
  <table width="75%" border="0" cellspacing="2" cellpadding="2">
    <tbody>
      <tr>
          <th width="18%" scope="col"><input name="button" type="button" class="boton" id="button" onclick="listado()" value="Listado"></th>
        <th width="20%" scope="col"><input name="submit" type="submit" class="boton" id="submit" value="Eliminar">
        <input name="op" type="hidden" id="op" value="eliminar"></th>
        <th width="23%" scope="col"><input name="button2" type="button" class="boton" id="button2" value="Buscar" onClick="buscar()"></th>
        <th width="21%" scope="col"><input name="button3" type="button" class="boton" id="button3" value="Insertar" onClick="insertar()"></th>
        <th width="9%" scope="col">&nbsp;</th>
        <th width="9%" scope="col">&nbsp;</th>
      </tr>
    </tbody>
  </table>
  <br>
  <% if(x!=null ) {
      if(x.equals("Buscar")) { %>      
  <table width="81%" border="0" cellpadding="0">
    <tbody>
      <tr>
        <th width="15%" scope="col">Buscar por</th>
        <th width="18%" scope="col"><select name="cbocampo" id="cbocampo">
          <option>Seleccionar</option>
          <option value="chr_emplcodigo">Codigo</option>
          <option value="vch_emplpaterno">Apellido Paterno</option>
          <option value="vch_emplmaterno">Apellido Materno</option>
          <option value="vch_emplnombre">Nombres</option>
          <option value="vch_emplciudad">Ciudad</option>
          <option value="vch_empldireccion">Dirección</option>
          <option value="vch_emplusuario">Usuario</option>
        </select></th>
        <th width="28%" scope="col">con el siguiente contenido</th>
        <th width="30%" scope="col"><input type="text" name="txtdato" id="txtdato">
        <input name="opb" type="hidden" id="opb" value="buscar"></th>
        <th width="9%" scope="col"><input name="submit2" type="submit" class="boton" id="submit2" value="Realizar Busqueda"></th>
      </tr>
    </tbody>
  </table>    
<% } } %>
  <br>
  <% if(rs!=null) { %>
  <table width="75%" border="0" cellspacing="2" cellpadding="2">
    <tbody>
      <tr>
        <th scope="col">&nbsp;</th>
        <th scope="col">Codigo</th>
        <th scope="col">Paterno</th>
        <th scope="col">Materno</th>
        <th scope="col">Nombre</th>
        <th scope="col">Ciudad</th>
        <th scope="col">Direccion</th>
        <th scope="col">Usuario</th>
      </tr>
      <% while(rs.next()) { %>
      <tr>
        <td align="center"><input type="checkbox" name="checks[]" id="checks[]" value="<%=rs.getString(1)%>"></td>
        <td><a href="/SistemaWebJSP/EmpleadoController?op=cargar&cod=<%=rs.getString(1)%>"><%=rs.getString(1)%></a>
        </td>
        <td><%=rs.getString(2)%></td>
        <td><%=rs.getString(3)%></td>
        <td><%=rs.getString(4)%></td>
        <td><%=rs.getString(5)%></td>
        <td><%=rs.getString(6)%></td>
        <td><%=rs.getString(7)%></td>
      </tr>
      <% } %>
    </tbody>
  </table>
  <% } %>
</form>
<br>
<%if(error!=null) { %>
<div id="error"><%=error%></div>
<% } %>
<p>&nbsp;</p>
<% if(x!=null && x.equals("Insertar")) { %>
<form action="/SistemaWebJSP/EmpleadoController" method="post" name="form2" id="form2">
  <table width="50%" border="0" cellpadding="0">
    <tbody>
      <tr>
        <th colspan="2" scope="col">INSERTAR EMPLEADO
        <input name="op" type="hidden" id="op" value="insertar"></th>
      </tr>
      <tr>
        <td width="30%">Apellido Paterno</td>
        <td width="70%"><input type="text" name="texto[]" id="texto[]"></td>
      </tr>
      <tr>
        <td>Apellido Materno</td>
        <td><input type="text" name="texto[]" id="texto[]"></td>
      </tr>
      <tr>
        <td>Nombres</td>
        <td><input type="text" name="texto[]" id="texto[]"></td>
      </tr>
      <tr>
        <td>Ciudad</td>
        <td><input type="text" name="texto[]" id="texto[]"></td>
      </tr>
      <tr>
        <td>Direccion</td>
        <td><input type="text" name="texto[]" id="texto[]"></td>
      </tr>
      <tr>
        <td>Usuario</td>
        <td><input type="text" name="texto[]" id="texto[]"></td>
      </tr>
      <tr>
        <td>Clave</td>
        <td><input type="text" name="texto[]" id="texto[]"></td>
      </tr>
      <tr>
        <td colspan="2" align="center"><input name="submit3" type="submit" class="boton" id="submit3" value="Enviar"></td>
      </tr>
    </tbody>
  </table>
</form>
<% } else if(rsc!=null) { %>
<form action="/SistemaWebJSP/EmpleadoController" method="post" name="form2" id="form2">
  <table width="50%" border="0" cellpadding="0">
    <tbody>
      <tr>
        <th colspan="2" scope="col">MODIFICAR EMPLEADO<input name="cod" type="hidden" id="op" value="<%=rsc.getString(1)%>">
        <input name="op" type="hidden" id="op" value="modificar"></th>
      </tr>
      <tr>
        <td width="30%">Apellido Paterno</td>
        <td width="70%"><input type="text" name="texto[]" id="texto[]" value="<%=rsc.getString(2)%>"></td>
      </tr>
      <tr>
        <td>Apellido Materno</td>
        <td><input type="text" name="texto[]" id="texto[]"  value="<%=rsc.getString(3)%>"></td>
      </tr>
      <tr>
        <td>Nombres</td>
        <td><input type="text" name="texto[]" id="texto[]"  value="<%=rsc.getString(4)%>"></td>
      </tr>
      <tr>
        <td>Ciudad</td>
        <td><input type="text" name="texto[]" id="texto[]"  value="<%=rsc.getString(5)%>"></td>
      </tr>
      <tr>
        <td>Direccion</td>
        <td><input type="text" name="texto[]" id="texto[]"  value="<%=rsc.getString(6)%>"></td>
      </tr>
      <tr>
        <td>Usuario</td>
        <td><input type="text" name="texto[]" id="texto[]"  value="<%=rsc.getString(7)%>"></td>
      </tr>
      <tr>
        <td>Clave</td>
        <td><input type="text" name="texto[]" id="texto[]"  value="<%=rsc.getString(8)%>"></td>
      </tr>
      <tr>
        <td colspan="2" align="center"><input name="submit3" type="submit" class="boton" id="submit3" value="Enviar"></td>
      </tr>
    </tbody>
  </table>
</form>
<% } %>
</body>
</html>









