<%@page import="java.sql.ResultSet"%>
<!doctype html>
<%
//habilitar el uso de sesiones
    HttpSession s=request.getSession();
    ResultSet rs=null;
    if(((ResultSet)s.getAttribute("sconstancia"))!=null){//si tiene data lo recupero
        rs=(ResultSet)s.getAttribute("sconstancia");//recuperar la data
        //s.removeAttribute("sconstancia");//borrar variable de sesion
    }
    String error=null;
    if(((String)s.getAttribute("serror"))!=null){//si tiene data?
        error=(String)s.getAttribute("serror");//recuperar la data
        s.removeAttribute("serror");//borrar la variable de sesion
    }
%>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<script language="javascript">
    function crearpdf(cta,nro){
        //invocar al servlet (reporteador)
        location.href="/SistemaWebJSP/rptConstancia?cuenta="+cta+"&numero="+nro;
    }
</script>
<link rel="stylesheet" type="text/css" href="../css/estilo.css">
<body>
<h1>DEPOSITO</h1>
<%if(rs==null){ //si aun no hay data en la operacion%>
<form action="/SistemaWebJSP/DepositoController" method="post" name="form1" id="form1">
  <table width="50%" border="1">
    <tbody>
      <tr>
        <th height="21" colspan="2" scope="col">EUREKABANK  a tu servicio - DEPOSITO</th>
      </tr>
      <tr>
        <td width="27%">Codigo de la Cuenta</td>
        <td width="73%"><input name="txtcta" type="text" autofocus required="required" id="txtcta" placeholder="codigo de la cuenta" pattern="^[0-9]{8}$" tabindex="1" autocomplete="on"></td>
      </tr>
      <tr>
        <td>Monto</td>
        <td><input name="txtmonto" type="text" required="required" id="txtmonto" placeholder="importe del deposito" pattern="^[0-9]+$" tabindex="2" title="no se acepta valores negativos ni nulos" autocomplete="on" value="0" maxlength="6"></td>
      </tr>
      <tr>
        <td colspan="2" align="center"><input name="submit" type="submit" class="boton" id="submit" tabindex="3" value="Enviar"></td>
      </tr>
    </tbody>
  </table>
</form>
<%}%>
<% if(rs!=null){
	rs.next(); %>
<table width="50%" border="2" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <th colspan="2" scope="col">CONSTANCIA DE LAS OPERACIONES</th>
    </tr>
    <tr>
      <td width="28%">EurekaBank</td>
      <td width="72%" align="right"><%=rs.getString(2)%></td>
    </tr>
    <tr>
      <td>Tu banco amigo</td>
      <td align="right"><%=rs.getString(13)%></td>
    </tr>
    <tr>
      <td colspan="2" align="center">************<%=rs.getString(6)%>*************</td>
    </tr>
    <tr>
      <td>Monto de Transaccion</td>
      <td align="right"><%=rs.getString(5)%></td>
    </tr>
    <tr>
      <td>Tipo de Moneda</td>
      <td align="right"><%=rs.getString(7)%></td>
    </tr>
    <tr>
      <td>ITF</td>
      <td align="right"><%=Double.parseDouble(rs.getString(5))*0.00005%></td>
    </tr>
    <tr>
      <td>Saldo Contable</td>
      <td align="right"><%=rs.getString(9)%></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Nro de Operacion</td>
      <td align="right"><%=rs.getString(4)%></td>
    </tr>
    <tr>
      <td>Cod Cuenta</td>
      <td align="right"><%=rs.getString(1)%></td>
    </tr>
    <tr>
      <td>Cliente :</td>
      <td align="right"><%=rs.getString(10)+""+rs.getString(10)+","+rs.getString(12)%></td>
    </tr>
    <tr>
      <td>Empleado :</td>
      <td align="right"><%=rs.getString(14)+""+rs.getString(15)+","+rs.getString(16)%></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="center"><input name="button" type="button" class="boton" id="button" value="Imprimir" onclick="javascript:print()"></td>
        <td align="center"><input name="button2" type="button" class="boton" id="button2" value="Generar PDF" onclick="crearpdf('<%=rs.getString(1)%>','<%=rs.getString(4)%>')"></td>
    </tr>
  </tbody>
</table>
<p>
  <%}%>
  <%if(error!=null){%>
<div id="error">ERROR: <%=error%></div>
<%}%>
</body>
</html>
