/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.MovimientosDAO;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ALUMNO
 */
@WebServlet(name = "rptConstancia", urlPatterns = {"/rptConstancia"})
public class rptConstancia extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            ResultSet rs=null;
         
            try {
    String cta=null;
    cta=request.getParameter("cuenta");
    int nro=0;
    nro=Integer.parseInt(request.getParameter("numero"));        
    //Mostrar los valores enviados
   // out.println("Cuenta : "+cta);
  //  out.println("Numero : "+nro);
                MovimientosDAO md=new MovimientosDAO();
                rs=md.mostrarConstancia(nro, cta);
                rs.next();
             
            //Construir los elementos del contenido de la pagina
            String cabecera="CONSTANCIA DE OPERACIONES\nEurekabank"
                    + "\t\t\t\t"+rs.getString(2)+"\nSucursal\t\t\t\t"+rs.getString(13); 
            String titulo="\n\n"+rs.getString(6)+"\n";
            //definir el tipo de letra para nuestro reporte
            Font fuente= new Font(Font.getFamily("ARIAL"), 12, Font.BOLD);
            Font fuente2= new Font(Font.getFamily("ARIAL"), 10, Font.BOLD);
            //Ensamblar los parrafos
            Paragraph linea = new Paragraph(cabecera,fuente);
            Paragraph linea2 = new Paragraph(titulo,fuente2);
             //crear una tabla
            PdfPTable tabla=new PdfPTable(2);
            tabla.setWidthPercentage(80);            
            //Creamos el documento
            Document documento = new Document(PageSize.A5);
            //Definimos la ruta de destino en donde se generará el archivo PDF
            String file="./ReporteConstancia.pdf";
            FileOutputStream archivo=new FileOutputStream(file);
            //Generamos el archivo PDF : Escribimos contenidos               
            PdfWriter.getInstance(documento,  archivo);
            //Renderizamos el contenido del documento
            //Creamos las celdas de cabecera
               PdfPCell celda1 =new PdfPCell (new Paragraph("Descripcion",FontFactory.getFont("arial",10,Font.BOLD,BaseColor.RED)));;
               PdfPCell celda2 =new PdfPCell (new Paragraph("Contenido",FontFactory.getFont("arial",10,Font.BOLD,BaseColor.RED)));
               //Agregar elemento al documento
               documento.open();               
                documento.add(linea);
                documento.add(linea2);
                //Añadir celdas a la tabla
                tabla.addCell(celda1);
                tabla.addCell(celda2);
                //Mas contenido
                tabla.addCell("Monto ");
                tabla.addCell(rs.getString(5));
                tabla.addCell("Moneda ");
                tabla.addCell(rs.getString(7));
               //Agregamos la tabla al documento
                documento.add(tabla);
                //cerrar el archivo documento
                documento.close();
            } catch (Exception e) {
                out.println("Error: "+e.getMessage());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}


