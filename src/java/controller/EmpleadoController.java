/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.EmpleadoModel;

/**
 *
 * @author ALUMNO
 */
@WebServlet(name = "EmpleadoController", urlPatterns = {"/EmpleadoController"})
public class EmpleadoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            //Contenido del controller
            //Obtener la sesion web
            HttpSession s=request.getSession();
            String[] cods=new String[200];
            String[] datos=new String[7];
            ResultSet rsc=null;
            try{
                //capturar peticion
                if(request.getParameterValues("texto[]")!=null){
                datos=request.getParameterValues("texto[]");  //capturar datos del form insertar
                }
                //capturar el codigo del registro a cargar
                String vcod=null;
                if(request.getParameter("cod")!=null){
                vcod=request.getParameter("cod");
                }
                String opc=null;
                if(request.getParameter("op")!=null){
                opc=request.getParameter("op");
                }
                String opcb=null;
                if(request.getParameter("opb")!=null){
                 opcb=request.getParameter("opb");  //Buscar
                }
                String campo=request.getParameter("cbocampo");
                String dato=request.getParameter("txtdato");
                   //out.println("Prueba 1 : "+opcb+" - Prueba 2 : "+opc);
                //capturar los values de los checks
                cods=request.getParameterValues("checks[]");
                //Instanciar a model
                EmpleadoModel em=new EmpleadoModel();
                ResultSet rs=null;
                if(opcb!=null){
                if(opcb.equalsIgnoreCase("buscar")) { // fin del if                   
                    rs=em.buscar(campo, dato);
                    opc=null;
                }
                }
                if(opc!=null){
                if(opc.equals("listado")){                    
                    rs=em.listado();                    
                }
                else if(opc.equals("eliminar")){
                  em.eliminar(cods);
                } 
                else if(opc.equals("insertar")){
                    em.insertar(datos);
                }
                else if(opc.equals("cargar")){
                    rsc=em.cargar(vcod);
                }
                else if(opc.equals("modificar")){
                    em.modificar(datos, vcod);
                }
                }
                //Guardar el esquema de datos en una variable de sesion
                s.setAttribute("sdata", rs);
                s.setAttribute("scarga", rsc);
                //regresar al view
                response.sendRedirect("/SistemaWebJSP/view/empleado.jsp");
            }
            catch(Exception ex){                
               //Guardar el mensaje de error en una variable de sesion
                s.setAttribute("serror",ex.getMessage());
                response.sendRedirect("/SistemaWebJSP/view/empleado.jsp");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
