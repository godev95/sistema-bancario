/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ds;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;

/**
 *
 * @author ALUMNO
 */
public class GestionBD {
    public static GestionBD instancia=null;  //variab estatica
    public Connection cn=null;  //propiedad
    
    //Patron de diseño Singleton
    public static GestionBD getInstancia() throws Exception {
        if (instancia==null) {
            instancia=new GestionBD();
        }
        return instancia;
    }
    
    //Metodo constructor
    public GestionBD() throws Exception {
        String servidor="localhost";
        String puerto="3306";
        String usuario="root";
        String password="";
        String bd="Eurekabank";
        String url="com.mysql.jdbc.Driver";
        String cadenaConex="jdbc:mysql://"+servidor+":"+puerto+"/"+bd;
        
        //Cargar el Driver
            Class.forName(url).newInstance();
            //Abrir la conexion
            cn=DriverManager.getConnection(cadenaConex,usuario,password);
    }
    
       public ResultSet ejecutarConsultaP(String sql,String[] params) throws Exception {
          PreparedStatement ps=cn.prepareStatement(sql);
          for(int i=0;i<params.length;i++){
          //Alimentar parametros
          ps.setString(i+1, params[i]);
          }
          //Ejecutar el comando
        ResultSet rs=ps.executeQuery();
        return rs;
    }  
      public ResultSet ejecutarConsultaP(String sql,Object[] params) throws Exception {
          PreparedStatement ps=cn.prepareStatement(sql);
          for(int i=0;i<params.length;i++){
          //Alimentar parametros
          ps.setObject(i+1, params[i]);
          }
          //Ejecutar el comando
        ResultSet rs=ps.executeQuery();
        return rs;
    }
      //Ejecutar procedimiento almacenado con parametros de salida
      public String ejecutarConsultaCPs(String sql,String[] params) throws Exception {
        CallableStatement cs=null;
        cs=cn.prepareCall(sql);
        for(int i=0;i<params.length;i++){
            cs.registerOutParameter(i+1,Types.CHAR);
        }
        cs.executeQuery();
        return cs.getString(1);
    }
      //Ejecutar un procedimiento almacenado que genera una consulta
      //con parametros de entrada
      public ResultSet ejecutarConsultaCPe(String sql,String[] params) throws Exception {
        CallableStatement cs=null;
        cs=cn.prepareCall(sql);
        for(int i=0;i<params.length;i++){
            cs.setString(i+1, params[i]);
        }
        ResultSet rs=cs.executeQuery();
        return rs;
    }
      //Ejecutar un procedimiento almacenado que genera una consulta
      //sin parametros
    public ResultSet ejecutarConsultaC(String sql) throws Exception {
        CallableStatement cs=null;
        cs=cn.prepareCall(sql);
        ResultSet rs=cs.executeQuery();
        return rs;
    }
      
      public ResultSet ejecutarConsulta(String sql) throws Exception {
        Statement st=cn.createStatement();
        ResultSet rs=st.executeQuery(sql);
        return rs;
    }
     public int ejecutarActualizacionP(String sql,String[] params) throws Exception {
       PreparedStatement ps=cn.prepareStatement(sql);
       for(int i=0;i<params.length;i++){
           ps.setString(i+1, params[i]);
       }
        int x=ps.executeUpdate();
        return x;
    }
      
      
    public int ejecutarActualizacionP(String sql,Object[] params) throws Exception {
       PreparedStatement ps=cn.prepareStatement(sql);
       for(int i=0;i<params.length;i++){
           ps.setObject(i+1, params[i]);
       }
        int x=ps.executeUpdate();
        return x;
    }
    
    public int ejecutarActualizacion(String sql) throws Exception {
        Statement st=cn.createStatement();
        int x=st.executeUpdate(sql);
        return x;
    }
}








