/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import DAO.EmpleadoDAO;
import java.sql.ResultSet;

/**
 *
 * @author ALUMNO
 */
public class EmpleadoModel {
    public void modificar(String datos[],String cod) throws Exception {
        EmpleadoDAO ed=new EmpleadoDAO();
        int x=ed.modificar(datos, cod);
        if(x==0){
            throw new Exception("No se modificó ningún registro...");
        }
    }
    public ResultSet cargar(String cod) throws Exception {
        EmpleadoDAO ed=new EmpleadoDAO();
        ResultSet rs=ed.cargar(cod);
        return rs;
    }
    public void insertar(String datos[]) throws Exception {
        EmpleadoDAO ed=new EmpleadoDAO();
        int x=ed.insertar(datos);
        if(x==0){
            throw new Exception("No se insertó el registro...");
        }
    }
       public ResultSet buscar(String campo,String dato)throws Exception{
         ResultSet rs=null;  
        EmpleadoDAO ed=new EmpleadoDAO();
        rs=ed.buscar(campo, dato);
        if(rs==null){
            throw new Exception("No se encontraron datos...");
        }
        return rs;
    }
    public void eliminar(String[] cods) throws Exception {
        int x=0;
        EmpleadoDAO ed=new EmpleadoDAO();
         x=ed.eliminar(cods);       
        //if(x==0){
          //  throw new Exception("Error: No seleccionó ningun registro...");
        //}
    }
    public ResultSet listado()throws Exception{
        EmpleadoDAO ed=new EmpleadoDAO();
        ResultSet rs=ed.listado();
        return rs;
    }
}
