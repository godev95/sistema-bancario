package model;


import DAO.EmpleadoDAO;
import DAO.MovimientosDAO;
import java.sql.ResultSet;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author UPT-28
 */
public class CuentaModel {
    public ResultSet deposito(String cta,double monto)throws Exception{
        ResultSet rs=null;
        MovimientosDAO md=new MovimientosDAO();
        rs=md.deposito(cta, monto);
        return rs;
    }
    public ResultSet mostrarMovimientos(String cta) throws Exception {
        MovimientosDAO md=new MovimientosDAO();
        ResultSet rs=md.mostrarMovimientos(cta);
        return rs;  
    }
}
