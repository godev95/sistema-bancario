/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import ds.GestionBD;
import java.sql.ResultSet;

/**
 *
 * @author ALUMNO
 */
public class EmpleadoDAO {
    //definir el comando sql
    
    public int modificar(String[] datos,String cod) throws Exception {
        String sql="update empleado set vch_emplpaterno=?,vch_emplmaterno=?,"
                + "vch_emplnombre=?,vch_emplciudad=?,vch_empldireccion=?,"
                + "vch_emplusuario=?,vch_emplclave=? where "
                + "chr_emplcodigo=?";
    String params[]=new String[]{datos[0],datos[1],datos[2],datos[3],datos[4],datos[5],datos[6],cod};    
    GestionBD gbd=GestionBD.getInstancia();
    int x=gbd.ejecutarActualizacionP(sql, params);
    return x;
    }
    public ResultSet cargar(String cod) throws Exception {
        String sql="select * from empleado where chr_emplcodigo=?";
        String params[]=new String[]{cod};
        GestionBD gbd=GestionBD.getInstancia();
        ResultSet rs=gbd.ejecutarConsultaP(sql, params);
        return rs;
    }
    public int insertar(String[] datos) throws Exception {
        String sql="insert into empleado values (?,?,?,?,?,?,?,?,?,?)";
        String cod=this.generarCodigo();
        String params[]=new String[]{cod,datos[0],datos[1],datos[2],datos[3],datos[4],datos[5],datos[6],"1",""};
        GestionBD gbd=GestionBD.getInstancia();
        int x=gbd.ejecutarActualizacionP(sql, params);
        return x;
    }
     public String generarCodigo() throws Exception {
        String sql = "{call usp_generaCodigo(?)}";        
        String params[]=new String[]{"@x"};
        //Conseguir la instania
        GestionBD gbd = GestionBD.getInstancia();
        String cod = gbd.ejecutarConsultaCPs(sql,params);        
        return cod;
    }
     public ResultSet buscar(String campo,String dato) throws Exception {
        String sql = "{call usp_buscar_empl(?,?)}";
        String params[]=new String[]{campo,dato};
        //Conseguir la instania
        GestionBD gbd = GestionBD.getInstancia();
        ResultSet rs = gbd.ejecutarConsultaCPe(sql,params);
        return rs;
    }
    
    public int eliminar(String[] cods) throws Exception {
        //Conseguir la instania
        GestionBD gbd = GestionBD.getInstancia();
        int x=0;                
        String params[]=new String[2];  //declaracion e inicializacion
        for(int i=0;i<cods.length;i++){          
         params[0]="0";   
         params[1]=cods[i];
        String sql = "update empleado set chr_emplestado=?"
                + " where chr_emplcodigo=?";
          x+=gbd.ejecutarActualizacionP(sql,params);          
    }// fin del for        
        return x;
    }
    
    public ResultSet listado() throws Exception {
        String sql = "select * from empleado where chr_emplestado=?";
        String params[]=new String[]{"1"};
        //Conseguir la instania
        GestionBD gbd = GestionBD.getInstancia();
        ResultSet rs = gbd.ejecutarConsultaP(sql,params);
        return rs;
    }
}
