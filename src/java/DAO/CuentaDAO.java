/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import ds.GestionBD;

/**
 *
 * @author UPT
 */
public class CuentaDAO {
    public void actualizarCantMovimientos(String cta,int cant)throws Exception{
        String sql="update cuenta set int_cuencontmov=int_cuencontmov+? "
                + "where chr_cuencodigo=?";
        
        Object [] params=new Object[]{cant,cta};
        
        GestionBD gbd=GestionBD.getInstancia();
        gbd.ejecutarActualizacionP(sql, params);
    }
    public void actualizarSaldo(String cta,String tipo,double monto)throws Exception{
        String sql="";
        if(tipo.equals("+")){
        sql="update cuenta set dec_cuensaldo=dec_cuensaldo+? where "
                + "chr_cuencodigo=?";
        }else if(tipo.equals("-")){
        sql="update cuenta set dec_cuensaldo=dec_cuensaldo-? where "
                + "chr_cuencodigo=?";    
        }
        Object [] params=new Object[]{monto,cta};
        GestionBD gbd=GestionBD.getInstancia();
        gbd.ejecutarActualizacionP(sql, params);
    }
            
}
