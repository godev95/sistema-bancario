/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import ds.GestionBD;
import java.sql.ResultSet;
import model.Fechas;
import model.TributariosModel;

/**
 *
 * @author UPT-28
 */
public class MovimientosDAO {

    /**
     * genera el numero de movimiento para un movimiento nuevo
     *
     * @param cta CodigoCta
     * @return NroMovimiento
     * @throws Exception
     */
    public ResultSet mostrarConstancia(int nromov,String cta)throws Exception{
        String sql="select * from vdeposito where chr_cuencodigo=? and int_movinumero=?";
        Object params[] = new Object[]{cta,nromov};
        GestionBD gbd = GestionBD.getInstancia();
        ResultSet rs = gbd.ejecutarConsultaP(sql,params);
        return rs;
    }
    public int generaNroMov(String cta) throws Exception {

        String sql = "select int_movinumero+1 from movimiento "
                + "where chr_cuencodigo=? order by int_movinumero DESC "
                + "limit 0,1";
        String params[] = new String[]{cta};
        GestionBD gbd = GestionBD.getInstancia();
        ResultSet rs = gbd.ejecutarConsultaP(sql, params);
        rs.next();//situarse en la primera fial dsponible
        return rs.getInt(1);
    }

    /**
     * Este metodo registra un movimiento en la tabla movimiento de la base de
     * datos Eurekabank
     *
     * @return int cantidad de registroInsertados
     */
    public int registrarMovimiento(String cta, String tipo, String codempl, double importe) throws Exception {
        String sql = "insert into movimiento values(?,?,?,?,?,?,?)";
        int nromov = this.generaNroMov(cta);
        String fecha = Fechas.devolverFechaActual();
        Object params[] = new Object[]{cta, nromov, fecha, codempl, tipo, importe, "(null)"};
        GestionBD gbd = GestionBD.getInstancia();
        int x = gbd.ejecutarActualizacionP(sql, params);
        return nromov;
    }

    public ResultSet deposito(String codcta, double monto) throws Exception {
        ResultSet rs = null;
        //Instanciar CuentaDAO
        CuentaDAO cd=new CuentaDAO();
        //Insertar el movimiento deposito
        int x=this.registrarMovimiento(codcta, "003", "0001", monto);
        //Insertar movimiento itf
        double itf=TributariosModel.CalcularITF(monto);
        int y=this.registrarMovimiento(codcta, "007", "0001",itf);
        //Actualizar el saldo por el deposito
        cd.actualizarSaldo(codcta,"+", monto);
        //Actualizar el saldo por itf
        cd.actualizarSaldo(codcta, "-", itf);
        //Incrementar la cantidad total de movimientos en la tabla cuentas
        cd.actualizarCantMovimientos(codcta, 2);
        //Generar la data constancia
        rs=this.mostrarConstancia(x, codcta);
        return rs;
    }

    public ResultSet mostrarMovimientos(String codcta) throws Exception {
        String sql = "{call usp_movis(?)}";
        String params[] = new String[]{codcta};
        //Conseguir la instania
        GestionBD gbd = GestionBD.getInstancia();
        ResultSet rs = gbd.ejecutarConsultaCPe(sql, params);
        return rs;
    }
}
