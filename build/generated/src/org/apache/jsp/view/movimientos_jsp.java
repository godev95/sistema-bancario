package org.apache.jsp.view;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;

public final class movimientos_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=utf-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!doctype html>\r\n");
 
    HttpSession s=request.getSession();
//rescatar los datos desde las variables de sesion
    ResultSet rs=null;
    if(((ResultSet)s.getAttribute("smovis"))!=null){
        rs=(ResultSet)s.getAttribute("smovis");
        s.removeAttribute("smovis");
    }
    String error=null;
    if(((String)s.getAttribute("serror"))!=null){
        error=(String)s.getAttribute("serror");
        s.removeAttribute("serror");
    }

      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta charset=\"utf-8\">\r\n");
      out.write("<title>Documento sin título</title>\r\n");
      out.write("</head>\r\n");
      out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/estilo.css\">\r\n");
      out.write("<body>\r\n");
      out.write("<h1>Movimientos de una cuenta</h1>\r\n");
      out.write("<form id=\"form1\" name=\"form1\" method=\"post\" action=\"/SistemaWebJSP/MovimientosController\">\r\n");
      out.write("\r\n");
      out.write("<table width=\"50%\" border=\"0\" cellpadding=\"0\">\r\n");
      out.write("  <tbody>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <th colspan=\"2\" scope=\"col\">EUREKABANK</th>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td width=\"34%\">Ingreso Codigo de Cuenta</td>\r\n");
      out.write("      <td width=\"66%\"><input name=\"textfield\" type=\"text\" autofocus=\"autofocus\" id=\"textfield\" placeholder=\"Codigo de Cuenta\"></td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td colspan=\"2\" align=\"center\"><input name=\"submit\" type=\"submit\" class=\"boton\" id=\"submit\" value=\"Enviar\"></td>\r\n");
      out.write("    </tr>\r\n");
      out.write("  </tbody>\r\n");
      out.write("</table>\r\n");
      out.write("</form>\r\n");
      out.write("<p>&nbsp;</p>\r\n");
 if(rs!=null) { 
      out.write("\r\n");
      out.write("<table width=\"50%\" border=\"0\" cellpadding=\"0\">\r\n");
      out.write("  <tbody>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <th scope=\"col\">Cod Cta</th>\r\n");
      out.write("      <th scope=\"col\">Fecha</th>\r\n");
      out.write("      <th scope=\"col\">Descripcion </th>\r\n");
      out.write("      <th scope=\"col\">Importe</th>\r\n");
      out.write("    </tr>\r\n");
      out.write("    ");
 while(rs.next()){ 
      out.write("\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td>");
      out.print(rs.getString(1));
      out.write("</td>\r\n");
      out.write("      <td>");
      out.print(rs.getString(2));
      out.write("</td>\r\n");
      out.write("      <td>");
      out.print(rs.getString(3));
      out.write("</td>\r\n");
      out.write("      <td>");
      out.print(rs.getString(4));
      out.write("</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    ");
 } 
      out.write("\r\n");
      out.write("  </tbody>\r\n");
      out.write("</table>\r\n");
 } 
      out.write("\r\n");
      out.write("<br>\r\n");
 if(error!=null) { 
      out.write("\r\n");
      out.write("<div id=\"error\">");
      out.print(error);
      out.write("</div>\r\n");
 }  
      out.write("\r\n");
      out.write("<p>&nbsp;</p>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
