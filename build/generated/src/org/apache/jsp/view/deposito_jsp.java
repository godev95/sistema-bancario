package org.apache.jsp.view;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;

public final class deposito_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!doctype html>\r\n");

//habilitar el uso de sesiones
    HttpSession s=request.getSession();
    ResultSet rs=null;
    if(((ResultSet)s.getAttribute("sconstancia"))!=null){//si tiene data lo recupero
        rs=(ResultSet)s.getAttribute("sconstancia");//recuperar la data
        //s.removeAttribute("sconstancia");//borrar variable de sesion
    }
    String error=null;
    if(((String)s.getAttribute("serror"))!=null){//si tiene data?
        error=(String)s.getAttribute("serror");//recuperar la data
        s.removeAttribute("serror");//borrar la variable de sesion
    }

      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta charset=\"utf-8\">\r\n");
      out.write("<title>Documento sin tÃ­tulo</title>\r\n");
      out.write("</head>\r\n");
      out.write("<script language=\"javascript\">\r\n");
      out.write("    function crearpdf(cta,nro){\r\n");
      out.write("        //invocar al servlet (reporteador)\r\n");
      out.write("        location.href=\"/SistemaWebJSP/rptConstancia?cuenta=\"+cta+\"&numero=\"+nro;\r\n");
      out.write("    }\r\n");
      out.write("</script>\r\n");
      out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/estilo.css\">\r\n");
      out.write("<body>\r\n");
      out.write("<h1>DEPOSITO</h1>\r\n");
if(rs==null){ //si aun no hay data en la operacion
      out.write("\r\n");
      out.write("<form action=\"/SistemaWebJSP/DepositoController\" method=\"post\" name=\"form1\" id=\"form1\">\r\n");
      out.write("  <table width=\"50%\" border=\"1\">\r\n");
      out.write("    <tbody>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <th height=\"21\" colspan=\"2\" scope=\"col\">EUREKABANK  a tu servicio - DEPOSITO</th>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td width=\"27%\">Codigo de la Cuenta</td>\r\n");
      out.write("        <td width=\"73%\"><input name=\"txtcta\" type=\"text\" autofocus required=\"required\" id=\"txtcta\" placeholder=\"codigo de la cuenta\" pattern=\"^[0-9]{8}$\" tabindex=\"1\" autocomplete=\"on\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td>Monto</td>\r\n");
      out.write("        <td><input name=\"txtmonto\" type=\"text\" required=\"required\" id=\"txtmonto\" placeholder=\"importe del deposito\" pattern=\"^[0-9]+$\" tabindex=\"2\" title=\"no se acepta valores negativos ni nulos\" autocomplete=\"on\" value=\"0\" maxlength=\"6\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td colspan=\"2\" align=\"center\"><input name=\"submit\" type=\"submit\" class=\"boton\" id=\"submit\" tabindex=\"3\" value=\"Enviar\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("    </tbody>\r\n");
      out.write("  </table>\r\n");
      out.write("</form>\r\n");
}
      out.write('\r');
      out.write('\n');
 if(rs!=null){
	rs.next(); 
      out.write("\r\n");
      out.write("<table width=\"50%\" border=\"2\" cellpadding=\"2\" cellspacing=\"2\">\r\n");
      out.write("  <tbody>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <th colspan=\"2\" scope=\"col\">CONSTANCIA DE LAS OPERACIONES</th>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td width=\"28%\">EurekaBank</td>\r\n");
      out.write("      <td width=\"72%\" align=\"right\">");
      out.print(rs.getString(2));
      out.write("</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td>Tu banco amigo</td>\r\n");
      out.write("      <td align=\"right\">");
      out.print(rs.getString(13));
      out.write("</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td colspan=\"2\" align=\"center\">************");
      out.print(rs.getString(6));
      out.write("*************</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td>Monto de Transaccion</td>\r\n");
      out.write("      <td align=\"right\">");
      out.print(rs.getString(5));
      out.write("</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td>Tipo de Moneda</td>\r\n");
      out.write("      <td align=\"right\">");
      out.print(rs.getString(7));
      out.write("</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td>ITF</td>\r\n");
      out.write("      <td align=\"right\">");
      out.print(Double.parseDouble(rs.getString(5))*0.00005);
      out.write("</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td>Saldo Contable</td>\r\n");
      out.write("      <td align=\"right\">");
      out.print(rs.getString(9));
      out.write("</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td>&nbsp;</td>\r\n");
      out.write("      <td>&nbsp;</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td>Nro de Operacion</td>\r\n");
      out.write("      <td align=\"right\">");
      out.print(rs.getString(4));
      out.write("</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td>Cod Cuenta</td>\r\n");
      out.write("      <td align=\"right\">");
      out.print(rs.getString(1));
      out.write("</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td>Cliente :</td>\r\n");
      out.write("      <td align=\"right\">");
      out.print(rs.getString(10)+""+rs.getString(10)+","+rs.getString(12));
      out.write("</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td>Empleado :</td>\r\n");
      out.write("      <td align=\"right\">");
      out.print(rs.getString(14)+""+rs.getString(15)+","+rs.getString(16));
      out.write("</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td>&nbsp;</td>\r\n");
      out.write("      <td>&nbsp;</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td align=\"center\"><input name=\"button\" type=\"button\" class=\"boton\" id=\"button\" value=\"Imprimir\" onclick=\"javascript:print()\"></td>\r\n");
      out.write("        <td align=\"center\"><input name=\"button2\" type=\"button\" class=\"boton\" id=\"button2\" value=\"Generar PDF\" onclick=\"crearpdf('");
      out.print(rs.getString(1));
      out.write("','");
      out.print(rs.getString(4));
      out.write("')\"></td>\r\n");
      out.write("    </tr>\r\n");
      out.write("  </tbody>\r\n");
      out.write("</table>\r\n");
      out.write("<p>\r\n");
      out.write("  ");
}
      out.write("\r\n");
      out.write("  ");
if(error!=null){
      out.write("\r\n");
      out.write("<div id=\"error\">ERROR: ");
      out.print(error);
      out.write("</div>\r\n");
}
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
