package org.apache.jsp.view;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;

public final class empleado_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

//Capturar los valores enviados a esta pagina
String x=null;
if(request.getParameter("x")!=null){
   x=request.getParameter("x");
   //out.println("opcion x "+x);
}    
       
//habilitar el uso de variables de sesion
    HttpSession s=request.getSession();
    //rescatar los datos guardados en las variables de sesion
    ResultSet rs=null;    
	ResultSet rsc=null;
	if(s.getAttribute("scarga")!=null){
    rsc=(ResultSet)s.getAttribute("scarga");
	rsc.next(); //posicionar en la primera fila
    s.removeAttribute("scarga");
    }
    if(s.getAttribute("sdata")!=null){
    rs=(ResultSet)s.getAttribute("sdata");
    s.removeAttribute("sdata");
    }
    
    String error=null;    
    if(s.getAttribute("serror")!=null){
    error=(String)s.getAttribute("serror");
    s.removeAttribute("serror");
    }

      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <title>Empleados</title>\r\n");
      out.write("    </head>\r\n");
      out.write("    <script language=\"javascript\">\r\n");
      out.write("\tfunction insertar(){\r\n");
      out.write("\t\tlocation.href=\"empleado.jsp?x=Insertar\";\r\n");
      out.write("\t}\r\n");
      out.write("\tfunction buscar(){\r\n");
      out.write("\t\tlocation.href=\"empleado.jsp?x=Buscar\";\r\n");
      out.write("\t}\r\n");
      out.write("        function listado(){\r\n");
      out.write("            location.href=\"/SistemaWebJSP/EmpleadoController?op=listado\";\r\n");
      out.write("        }\r\n");
      out.write("        </script>\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"../css/estilo.css\">\r\n");
      out.write("<body>\r\n");
      out.write("        <h1>Empleados</h1>\r\n");
      out.write("        <br>\r\n");
      out.write("<form id=\"form1\" name=\"form1\" method=\"post\" action=\"/SistemaWebJSP/EmpleadoController\">\r\n");
      out.write("  <table width=\"75%\" border=\"0\" cellspacing=\"2\" cellpadding=\"2\">\r\n");
      out.write("    <tbody>\r\n");
      out.write("      <tr>\r\n");
      out.write("          <th width=\"18%\" scope=\"col\"><input name=\"button\" type=\"button\" class=\"boton\" id=\"button\" onclick=\"listado()\" value=\"Listado\"></th>\r\n");
      out.write("        <th width=\"20%\" scope=\"col\"><input name=\"submit\" type=\"submit\" class=\"boton\" id=\"submit\" value=\"Eliminar\">\r\n");
      out.write("        <input name=\"op\" type=\"hidden\" id=\"op\" value=\"eliminar\"></th>\r\n");
      out.write("        <th width=\"23%\" scope=\"col\"><input name=\"button2\" type=\"button\" class=\"boton\" id=\"button2\" value=\"Buscar\" onClick=\"buscar()\"></th>\r\n");
      out.write("        <th width=\"21%\" scope=\"col\"><input name=\"button3\" type=\"button\" class=\"boton\" id=\"button3\" value=\"Insertar\" onClick=\"insertar()\"></th>\r\n");
      out.write("        <th width=\"9%\" scope=\"col\">&nbsp;</th>\r\n");
      out.write("        <th width=\"9%\" scope=\"col\">&nbsp;</th>\r\n");
      out.write("      </tr>\r\n");
      out.write("    </tbody>\r\n");
      out.write("  </table>\r\n");
      out.write("  <br>\r\n");
      out.write("  ");
 if(x!=null ) {
      if(x.equals("Buscar")) { 
      out.write("      \r\n");
      out.write("  <table width=\"81%\" border=\"0\" cellpadding=\"0\">\r\n");
      out.write("    <tbody>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <th width=\"15%\" scope=\"col\">Buscar por</th>\r\n");
      out.write("        <th width=\"18%\" scope=\"col\"><select name=\"cbocampo\" id=\"cbocampo\">\r\n");
      out.write("          <option>Seleccionar</option>\r\n");
      out.write("          <option value=\"chr_emplcodigo\">Codigo</option>\r\n");
      out.write("          <option value=\"vch_emplpaterno\">Apellido Paterno</option>\r\n");
      out.write("          <option value=\"vch_emplmaterno\">Apellido Materno</option>\r\n");
      out.write("          <option value=\"vch_emplnombre\">Nombres</option>\r\n");
      out.write("          <option value=\"vch_emplciudad\">Ciudad</option>\r\n");
      out.write("          <option value=\"vch_empldireccion\">Dirección</option>\r\n");
      out.write("          <option value=\"vch_emplusuario\">Usuario</option>\r\n");
      out.write("        </select></th>\r\n");
      out.write("        <th width=\"28%\" scope=\"col\">con el siguiente contenido</th>\r\n");
      out.write("        <th width=\"30%\" scope=\"col\"><input type=\"text\" name=\"txtdato\" id=\"txtdato\">\r\n");
      out.write("        <input name=\"opb\" type=\"hidden\" id=\"opb\" value=\"buscar\"></th>\r\n");
      out.write("        <th width=\"9%\" scope=\"col\"><input name=\"submit2\" type=\"submit\" class=\"boton\" id=\"submit2\" value=\"Realizar Busqueda\"></th>\r\n");
      out.write("      </tr>\r\n");
      out.write("    </tbody>\r\n");
      out.write("  </table>    \r\n");
 } } 
      out.write("\r\n");
      out.write("  <br>\r\n");
      out.write("  ");
 if(rs!=null) { 
      out.write("\r\n");
      out.write("  <table width=\"75%\" border=\"0\" cellspacing=\"2\" cellpadding=\"2\">\r\n");
      out.write("    <tbody>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <th scope=\"col\">&nbsp;</th>\r\n");
      out.write("        <th scope=\"col\">Codigo</th>\r\n");
      out.write("        <th scope=\"col\">Paterno</th>\r\n");
      out.write("        <th scope=\"col\">Materno</th>\r\n");
      out.write("        <th scope=\"col\">Nombre</th>\r\n");
      out.write("        <th scope=\"col\">Ciudad</th>\r\n");
      out.write("        <th scope=\"col\">Direccion</th>\r\n");
      out.write("        <th scope=\"col\">Usuario</th>\r\n");
      out.write("      </tr>\r\n");
      out.write("      ");
 while(rs.next()) { 
      out.write("\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td align=\"center\"><input type=\"checkbox\" name=\"checks[]\" id=\"checks[]\" value=\"");
      out.print(rs.getString(1));
      out.write("\"></td>\r\n");
      out.write("        <td><a href=\"/SistemaWebJSP/EmpleadoController?op=cargar&cod=");
      out.print(rs.getString(1));
      out.write('"');
      out.write('>');
      out.print(rs.getString(1));
      out.write("</a>\r\n");
      out.write("        </td>\r\n");
      out.write("        <td>");
      out.print(rs.getString(2));
      out.write("</td>\r\n");
      out.write("        <td>");
      out.print(rs.getString(3));
      out.write("</td>\r\n");
      out.write("        <td>");
      out.print(rs.getString(4));
      out.write("</td>\r\n");
      out.write("        <td>");
      out.print(rs.getString(5));
      out.write("</td>\r\n");
      out.write("        <td>");
      out.print(rs.getString(6));
      out.write("</td>\r\n");
      out.write("        <td>");
      out.print(rs.getString(7));
      out.write("</td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      ");
 } 
      out.write("\r\n");
      out.write("    </tbody>\r\n");
      out.write("  </table>\r\n");
      out.write("  ");
 } 
      out.write("\r\n");
      out.write("</form>\r\n");
      out.write("<br>\r\n");
if(error!=null) { 
      out.write("\r\n");
      out.write("<div id=\"error\">");
      out.print(error);
      out.write("</div>\r\n");
 } 
      out.write("\r\n");
      out.write("<p>&nbsp;</p>\r\n");
 if(x!=null && x.equals("Insertar")) { 
      out.write("\r\n");
      out.write("<form action=\"/SistemaWebJSP/EmpleadoController\" method=\"post\" name=\"form2\" id=\"form2\">\r\n");
      out.write("  <table width=\"50%\" border=\"0\" cellpadding=\"0\">\r\n");
      out.write("    <tbody>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <th colspan=\"2\" scope=\"col\">INSERTAR EMPLEADO\r\n");
      out.write("        <input name=\"op\" type=\"hidden\" id=\"op\" value=\"insertar\"></th>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td width=\"30%\">Apellido Paterno</td>\r\n");
      out.write("        <td width=\"70%\"><input type=\"text\" name=\"texto[]\" id=\"texto[]\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td>Apellido Materno</td>\r\n");
      out.write("        <td><input type=\"text\" name=\"texto[]\" id=\"texto[]\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td>Nombres</td>\r\n");
      out.write("        <td><input type=\"text\" name=\"texto[]\" id=\"texto[]\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td>Ciudad</td>\r\n");
      out.write("        <td><input type=\"text\" name=\"texto[]\" id=\"texto[]\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td>Direccion</td>\r\n");
      out.write("        <td><input type=\"text\" name=\"texto[]\" id=\"texto[]\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td>Usuario</td>\r\n");
      out.write("        <td><input type=\"text\" name=\"texto[]\" id=\"texto[]\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td>Clave</td>\r\n");
      out.write("        <td><input type=\"text\" name=\"texto[]\" id=\"texto[]\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td colspan=\"2\" align=\"center\"><input name=\"submit3\" type=\"submit\" class=\"boton\" id=\"submit3\" value=\"Enviar\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("    </tbody>\r\n");
      out.write("  </table>\r\n");
      out.write("</form>\r\n");
 } else if(rsc!=null) { 
      out.write("\r\n");
      out.write("<form action=\"/SistemaWebJSP/EmpleadoController\" method=\"post\" name=\"form2\" id=\"form2\">\r\n");
      out.write("  <table width=\"50%\" border=\"0\" cellpadding=\"0\">\r\n");
      out.write("    <tbody>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <th colspan=\"2\" scope=\"col\">MODIFICAR EMPLEADO<input name=\"cod\" type=\"hidden\" id=\"op\" value=\"");
      out.print(rsc.getString(1));
      out.write("\">\r\n");
      out.write("        <input name=\"op\" type=\"hidden\" id=\"op\" value=\"modificar\"></th>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td width=\"30%\">Apellido Paterno</td>\r\n");
      out.write("        <td width=\"70%\"><input type=\"text\" name=\"texto[]\" id=\"texto[]\" value=\"");
      out.print(rsc.getString(2));
      out.write("\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td>Apellido Materno</td>\r\n");
      out.write("        <td><input type=\"text\" name=\"texto[]\" id=\"texto[]\"  value=\"");
      out.print(rsc.getString(3));
      out.write("\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td>Nombres</td>\r\n");
      out.write("        <td><input type=\"text\" name=\"texto[]\" id=\"texto[]\"  value=\"");
      out.print(rsc.getString(4));
      out.write("\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td>Ciudad</td>\r\n");
      out.write("        <td><input type=\"text\" name=\"texto[]\" id=\"texto[]\"  value=\"");
      out.print(rsc.getString(5));
      out.write("\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td>Direccion</td>\r\n");
      out.write("        <td><input type=\"text\" name=\"texto[]\" id=\"texto[]\"  value=\"");
      out.print(rsc.getString(6));
      out.write("\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td>Usuario</td>\r\n");
      out.write("        <td><input type=\"text\" name=\"texto[]\" id=\"texto[]\"  value=\"");
      out.print(rsc.getString(7));
      out.write("\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td>Clave</td>\r\n");
      out.write("        <td><input type=\"text\" name=\"texto[]\" id=\"texto[]\"  value=\"");
      out.print(rsc.getString(8));
      out.write("\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      <tr>\r\n");
      out.write("        <td colspan=\"2\" align=\"center\"><input name=\"submit3\" type=\"submit\" class=\"boton\" id=\"submit3\" value=\"Enviar\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("    </tbody>\r\n");
      out.write("  </table>\r\n");
      out.write("</form>\r\n");
 } 
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
