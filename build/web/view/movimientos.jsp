<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<!doctype html>
<% 
    HttpSession s=request.getSession();
//rescatar los datos desde las variables de sesion
    ResultSet rs=null;
    if(((ResultSet)s.getAttribute("smovis"))!=null){
        rs=(ResultSet)s.getAttribute("smovis");
        s.removeAttribute("smovis");
    }
    String error=null;
    if(((String)s.getAttribute("serror"))!=null){
        error=(String)s.getAttribute("serror");
        s.removeAttribute("serror");
    }
%>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<link rel="stylesheet" type="text/css" href="../css/estilo.css">
<body>
<h1>Movimientos de una cuenta</h1>
<form id="form1" name="form1" method="post" action="/SistemaWebJSP/MovimientosController">

<table width="50%" border="0" cellpadding="0">
  <tbody>
    <tr>
      <th colspan="2" scope="col">EUREKABANK</th>
    </tr>
    <tr>
      <td width="34%">Ingreso Codigo de Cuenta</td>
      <td width="66%"><input name="textfield" type="text" autofocus="autofocus" id="textfield" placeholder="Codigo de Cuenta"></td>
    </tr>
    <tr>
      <td colspan="2" align="center"><input name="submit" type="submit" class="boton" id="submit" value="Enviar"></td>
    </tr>
  </tbody>
</table>
</form>
<p>&nbsp;</p>
<% if(rs!=null) { %>
<table width="50%" border="0" cellpadding="0">
  <tbody>
    <tr>
      <th scope="col">Cod Cta</th>
      <th scope="col">Fecha</th>
      <th scope="col">Descripcion </th>
      <th scope="col">Importe</th>
    </tr>
    <% while(rs.next()){ %>
    <tr>
      <td><%=rs.getString(1)%></td>
      <td><%=rs.getString(2)%></td>
      <td><%=rs.getString(3)%></td>
      <td><%=rs.getString(4)%></td>
    </tr>
    <% } %>
  </tbody>
</table>
<% } %>
<br>
<% if(error!=null) { %>
<div id="error"><%=error%></div>
<% }  %>
<p>&nbsp;</p>
</body>
</html>