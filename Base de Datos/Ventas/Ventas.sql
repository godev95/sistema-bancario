/*
SQLyog Ultimate v8.53 
MySQL - 5.0.51b-community-nt-log : Database - ventas
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ventas` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci */;

USE `ventas`;

/*Table structure for table `productos` */

DROP TABLE IF EXISTS `productos`;

CREATE TABLE `productos` (
  `pro_cod` int(7) unsigned zerofill NOT NULL auto_increment,
  `pro_nomb` varchar(45) collate utf8_spanish2_ci default NULL,
  `pro_stock` int(6) default NULL,
  `pro_preciovp` double default NULL,
  `pro_marca` varchar(25) collate utf8_spanish2_ci default NULL,
  `pro_preciocosto` double default NULL,
  `pro_descrip` varchar(100) collate utf8_spanish2_ci default NULL,
  PRIMARY KEY  (`pro_cod`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*Data for the table `productos` */

insert  into `productos`(`pro_cod`,`pro_nomb`,`pro_stock`,`pro_preciovp`,`pro_marca`,`pro_preciocosto`,`pro_descrip`) values (0000001,'Huevos Pardo',100,4.9,'La Calera',4.2,'Huevos a granel'),(0000002,'Leche Evaporada',589,3.2,'Gloria',2.8,'Tarro de 410gr'),(0000003,'Mantequilla',300,8.9,'Laive',7.2,'Light'),(0000004,'Pan Frances',250,5.45,'Bimbo',3.2,'A granel'),(0000005,'Queso',210,20,'Laive',16,'Pasteurizado');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
