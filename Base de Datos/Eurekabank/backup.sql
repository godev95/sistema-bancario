/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.0.51b-community-nt-log : Database - eurekabank
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`eurekabank` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `eurekabank`;

/*Table structure for table `asignado` */

DROP TABLE IF EXISTS `asignado`;

CREATE TABLE `asignado` (
  `chr_asigcodigo` char(6) NOT NULL,
  `chr_sucucodigo` char(3) NOT NULL,
  `chr_emplcodigo` char(4) NOT NULL,
  `dtt_asigfechaalta` date NOT NULL,
  `dtt_asigfechabaja` date default NULL,
  PRIMARY KEY  (`chr_asigcodigo`),
  KEY `idx_asignado01` (`chr_emplcodigo`),
  KEY `idx_asignado02` (`chr_sucucodigo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `asignado` */

insert  into `asignado`(`chr_asigcodigo`,`chr_sucucodigo`,`chr_emplcodigo`,`dtt_asigfechaalta`,`dtt_asigfechabaja`) values ('000001','002','0001','0000-00-00','0000-00-00'),('000002','001','0003','0000-00-00','0000-00-00');

/*Table structure for table `cargomantenimiento` */

DROP TABLE IF EXISTS `cargomantenimiento`;

CREATE TABLE `cargomantenimiento` (
  `chr_monecodigo` char(2) NOT NULL,
  `dec_cargMontoMaximo` decimal(12,2) NOT NULL,
  `dec_cargImporte` decimal(12,2) NOT NULL,
  PRIMARY KEY  (`chr_monecodigo`),
  KEY `idx_cargomantenimiento01` (`chr_monecodigo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cargomantenimiento` */

insert  into `cargomantenimiento`(`chr_monecodigo`,`dec_cargMontoMaximo`,`dec_cargImporte`) values ('01',3500.00,7.00),('02',1200.00,2.50);

/*Table structure for table `cliente` */

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `chr_cliecodigo` char(5) NOT NULL,
  `vch_cliepaterno` varchar(25) NOT NULL,
  `vch_cliematerno` varchar(25) NOT NULL,
  `vch_clienombre` varchar(30) NOT NULL,
  `chr_cliedni` char(8) NOT NULL,
  `vch_clieciudad` varchar(30) NOT NULL,
  `vch_cliedireccion` varchar(50) NOT NULL,
  `vch_clietelefono` varchar(20) default NULL,
  `vch_clieemail` varchar(50) default NULL,
  `fotocli` varchar(50) default NULL,
  `vch_clieestado` varchar(1) NOT NULL default '1',
  PRIMARY KEY  (`chr_cliecodigo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cliente` */

insert  into `cliente`(`chr_cliecodigo`,`vch_cliepaterno`,`vch_cliematerno`,`vch_clienombre`,`chr_cliedni`,`vch_clieciudad`,`vch_cliedireccion`,`vch_clietelefono`,`vch_clieemail`,`fotocli`,`vch_clieestado`) values ('00001','CORONEL','CASTILLO','ERIC GUSTAVO','06914897','LIMA','LOS OLIVOS','9666-4457','gcoronel@viabcp.com','Cabello.jpg','1'),('00002','VALENCIA','MORALES','PEDRO HUGO','01576173','LIMA','MAGDALENA','924-7834','pvalencia@terra.com.pe','Gustavo.jpg','1'),('00003','MARCELO','VILLALOBOS','RICARDO','10762367','LIMA','LINCE','993-62966','ricardomarcelo@hotmail.com','Jonathan.jpg','1'),('00004','ROMERO','CASTILLO','CARLOS ALBERTO','06531983','LIMA','LOS OLIVOS','865-84762','c.romero@hotmail.com','Juan.jpg','1'),('00005','ARANDA','LUNA','ALAN ALBERTO','10875611','LIMA','SAN ISIDRO','834-67125','a.aranda@hotmail.com','Lino.jpg','1'),('00006','AYALA','PAZ','JORGE LUIS','10679245','LIMA','SAN BORJA','963-34769','j.ayala@yahoo.com','Lourdes.jpg','1'),('00007','CHAVEZ','CANALES','EDGAR RAFAEL','10145693','LIMA','MIRAFLORES','999-96673','e.chavez@gmail.com','Cabello.jpg','1'),('00008','FLORES','CHAFLOQUE','ROSA LIZET','10773456','LIMA','LA MOLINA','966-87567','r.florez@hotmail.com','Cabello.jpg','1'),('00009','FLORES','CASTILLO','CRISTIAN RAFAEL','10346723','LIMA','LOS OLIVOS','978-43768','c.flores@hotmail.com','Cabello.jpg','1'),('00010','GONZALES','GARCIA','GABRIEL ALEJANDRO','10192376','LIMA','SAN MIGUEL','945-56782','g.gonzales@yahoo.es','Cabello.jpg','1'),('00011','LAY','VALLEJOS','JUAN CARLOS','10942287','LIMA','LINCE','956-12657','j.lay@peru.com','Cabello.jpg','1'),('00012','MONTALVO','SOTO','DEYSI LIDIA','10612376','LIMA','SURCO','965-67235','d.montalvo@hotmail.com','Cabello.jpg','1'),('00013','RICALDE','RAMIREZ','ROSARIO ESMERALDA','10761324','LIMA','MIRAFLORES','991-23546','r.ricalde@gmail.com','Cabello.jpg','1'),('00014','RODRIGUEZ','FLORES','ENRIQUE MANUEL','10773345','LIMA','LINCE','976-82838','e.rodriguez@gmail.com','Cabello.jpg','1'),('00015','ROJAS','OSCANOA','FELIX NINO','10238943','LIMA','LIMA','962-32158','f.rojas@yahoo.com','Cabello.jpg','1'),('00016','TEJADA','DEL AGUILA','TANIA LORENA','10446791','LIMA','PUEBLO LIBRE','966-23854','t.tejada@hotmail.com','Cabello.jpg','1'),('00017','VALDEVIESO','LEYVA','LIDIA ROXANA','10452682','LIMA','SURCO','956-78951','r.valdivieso@terra.com.pe','Cabello.jpg','1'),('00018','VALENTIN','COTRINA','JUAN DIEGO','10398247','LIMA','LA MOLINA','921-12456','j.valentin@terra.com.pe','Cabello.jpg','1'),('00019','YAURICASA','BAUTISTA','YESABETH','10934584','LIMA','MAGDALENA','977-75777','y.yauricasa@terra.com.pe','Cabello.jpg','1'),('00020','ZEGARRA','GARCIA','FERNANDO MOISES','10772365','LIMA','SAN ISIDRO','936-45876','f.zegarra@hotmail.com','Cabello.jpg','1'),('0021','Arana','Sifuentes','jorge','47036896','lima','av.los dominicos','984722339','jorge_45_$5','arana.jpg','0'),('0022','Arana','Sifuentes','jorge','47036896','lima','av.los dominicos','984722339','jorge_45_$5','arana.jpg','0'),('0023','Arana','Sifuentes','enrique','14534466','lima','av.los dominicos','984722339','jorge_45_56@hotmail.com','arana.jpg','1'),('0024','Arana','Sifuentes','emilia','3213123','trujillo','av.santarosa','21312321','emilia@hotmail.com','arana.jpg','0');

/*Table structure for table `contador` */

DROP TABLE IF EXISTS `contador`;

CREATE TABLE `contador` (
  `vch_conttabla` varchar(30) NOT NULL,
  `int_contitem` int(11) NOT NULL,
  `int_contlongitud` int(11) NOT NULL,
  PRIMARY KEY  (`vch_conttabla`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `contador` */

insert  into `contador`(`vch_conttabla`,`int_contitem`,`int_contlongitud`) values ('Asignado',11,6),('Cliente',21,5),('Empleado',11,4),('Moneda',2,2),('Parametro',2,3),('Sucursal',7,3),('TipoMovimiento',10,3);

/*Table structure for table `costomovimiento` */

DROP TABLE IF EXISTS `costomovimiento`;

CREATE TABLE `costomovimiento` (
  `chr_monecodigo` char(2) NOT NULL,
  `dec_costimporte` decimal(12,2) NOT NULL,
  PRIMARY KEY  (`chr_monecodigo`),
  KEY `idx_costomovimiento` (`chr_monecodigo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `costomovimiento` */

insert  into `costomovimiento`(`chr_monecodigo`,`dec_costimporte`) values ('01',2.00),('02',0.60);

/*Table structure for table `cuenta` */

DROP TABLE IF EXISTS `cuenta`;

CREATE TABLE `cuenta` (
  `chr_cuencodigo` char(8) NOT NULL,
  `chr_monecodigo` char(2) NOT NULL,
  `chr_sucucodigo` char(3) NOT NULL,
  `chr_emplcreacuenta` char(4) NOT NULL,
  `chr_cliecodigo` char(5) NOT NULL,
  `dec_cuensaldo` decimal(12,2) NOT NULL,
  `dtt_cuenfechacreacion` date NOT NULL,
  `vch_cuenestado` varchar(15) NOT NULL default 'ACTIVO',
  `int_cuencontmov` int(11) NOT NULL,
  `chr_cuenclave` char(6) NOT NULL,
  PRIMARY KEY  (`chr_cuencodigo`),
  KEY `idx_cuenta01` (`chr_cliecodigo`),
  KEY `idx_cuenta02` (`chr_emplcreacuenta`),
  KEY `idx_cuenta03` (`chr_sucucodigo`),
  KEY `idx_cuenta04` (`chr_monecodigo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cuenta` */

insert  into `cuenta`(`chr_cuencodigo`,`chr_monecodigo`,`chr_sucucodigo`,`chr_emplcreacuenta`,`chr_cliecodigo`,`dec_cuensaldo`,`dtt_cuenfechacreacion`,`vch_cuenestado`,`int_cuencontmov`,`chr_cuenclave`) values ('00100001','01','001','0004','00005',6900.00,'2008-01-06','ACTIVO',7,'123456'),('00100002','02','001','0004','00005',4500.00,'2008-01-08','ACTIVO',4,'123456'),('00200001','01','002','0001','00008',7000.00,'2008-01-05','ACTIVO',15,'123456'),('00200002','01','002','0001','00001',6800.00,'2008-01-09','ACTIVO',3,'123456'),('00200003','02','002','0001','00007',6000.00,'2008-01-11','ACTIVO',6,'123456');

/*Table structure for table `empleado` */

DROP TABLE IF EXISTS `empleado`;

CREATE TABLE `empleado` (
  `chr_emplcodigo` char(4) NOT NULL,
  `vch_emplpaterno` varchar(25) NOT NULL,
  `vch_emplmaterno` varchar(25) NOT NULL,
  `vch_emplnombre` varchar(30) NOT NULL,
  `vch_emplciudad` varchar(30) NOT NULL,
  `vch_empldireccion` varchar(50) default NULL,
  `vch_emplusuario` varchar(15) NOT NULL,
  `vch_emplclave` varchar(45) NOT NULL,
  `chr_emplestado` char(1) default '1',
  `chr_emplfoto` char(8) default NULL,
  PRIMARY KEY  (`chr_emplcodigo`),
  UNIQUE KEY `U_Empleado_vch_emplusuario` (`vch_emplusuario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `empleado` */

insert  into `empleado`(`chr_emplcodigo`,`vch_emplpaterno`,`vch_emplmaterno`,`vch_emplnombre`,`vch_emplciudad`,`vch_empldireccion`,`vch_emplusuario`,`vch_emplclave`,`chr_emplestado`,`chr_emplfoto`) values ('0001','Romero','Castillo','Carlos Alberto','Trujillo','Call1 1 Nro. 456','cromero','b8cce5d4a2b4af118596fe2e26797bb5','1','0001.jpg'),('0002','Castro','Vargas','Lidia','Lima','Federico Villarreal 456 - SMP','lcastro','e0f3fa5da53e0a51f022027d7876b0e5','1','0002.jpg'),('0003','Reyes','Ortiz','Claudia','Lima','Av. Aviación 3456 - San Borja','aortiz','linda','1','0003.jpg'),('0004','Ramos','Garibay','Angelica','Chiclayo','Calle Barcelona 345','aramos','china','1','0004.jpg'),('0005','Ruiz','Zabaleta','Claudia','Cusco','Calle Cruz Verde 364','cvalencia','angel','1','0005.jpg'),('0006','Cruz','Tarazona','Ricardo','Areguipa','Calle La Gruta 304','rcruz','cerebro','1','0006.jpg'),('0007','Diaz','Flores','Edith','Lima','Av. Pardo 546','ediaz','princesa','1','0007.jpg'),('0008','Sarmiento','Bellido','Claudia Rocio','Areguipa','Calle Alfonso Ugarte 1567','csarmiento','chinita','1','0008.jpg'),('0009','Pachas','Sifuentes','Luis Alberto','Trujillo','Francisco Pizarro 1263','lpachas','gato','1','0009.jpg'),('0010','Tello','Alarcon','Hugo Valentin','Cusco','Los Angeles 865','htello','machupichu','1','0010.jpg'),('0011','Carrasco','Vargas','Pedro Hugo','Chiclayo','Av. Balta 1265','pcarrasco','tinajones','1','0011.jpg');

/*Table structure for table `interesmensual` */

DROP TABLE IF EXISTS `interesmensual`;

CREATE TABLE `interesmensual` (
  `chr_monecodigo` char(2) NOT NULL,
  `dec_inteimporte` decimal(12,2) NOT NULL,
  PRIMARY KEY  (`chr_monecodigo`),
  KEY `idx_interesmensual01` (`chr_monecodigo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `interesmensual` */

insert  into `interesmensual`(`chr_monecodigo`,`dec_inteimporte`) values ('01',0.70),('02',0.60);

/*Table structure for table `moneda` */

DROP TABLE IF EXISTS `moneda`;

CREATE TABLE `moneda` (
  `chr_monecodigo` char(2) NOT NULL,
  `vch_monedescripcion` varchar(20) NOT NULL,
  PRIMARY KEY  (`chr_monecodigo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `moneda` */

insert  into `moneda`(`chr_monecodigo`,`vch_monedescripcion`) values ('01','Soles'),('02','Dolares');

/*Table structure for table `movimiento` */

DROP TABLE IF EXISTS `movimiento`;

CREATE TABLE `movimiento` (
  `chr_cuencodigo` char(8) NOT NULL,
  `int_movinumero` int(11) NOT NULL,
  `dtt_movifecha` date NOT NULL,
  `chr_emplcodigo` char(4) NOT NULL,
  `chr_tipocodigo` char(3) NOT NULL,
  `dec_moviimporte` decimal(12,2) NOT NULL,
  `chr_cuenreferencia` char(8) default NULL,
  PRIMARY KEY  (`chr_cuencodigo`,`int_movinumero`),
  KEY `idx_movimiento01` (`chr_tipocodigo`),
  KEY `idx_movimiento02` (`chr_emplcodigo`),
  KEY `idx_movimiento03` (`chr_cuencodigo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `movimiento` */

insert  into `movimiento`(`chr_cuencodigo`,`int_movinumero`,`dtt_movifecha`,`chr_emplcodigo`,`chr_tipocodigo`,`dec_moviimporte`,`chr_cuenreferencia`) values ('00100001',1,'2008-01-06','0004','001',2800.00,NULL),('00100001',2,'2008-01-15','0004','003',3200.00,NULL),('00100001',3,'2008-01-20','0004','004',800.00,NULL),('00100001',4,'2008-02-14','0004','003',2000.00,NULL),('00100001',5,'2008-02-25','0004','004',500.00,NULL),('00100001',6,'2008-03-03','0004','004',800.00,NULL),('00100001',7,'2008-03-15','0004','003',1000.00,NULL),('00100002',1,'2008-01-08','0004','001',1800.00,NULL),('00100002',2,'2008-01-25','0004','004',1000.00,NULL),('00100002',3,'2008-02-13','0004','003',2200.00,NULL),('00100002',4,'2008-03-08','0004','003',1500.00,NULL),('00200001',1,'2008-01-05','0001','001',5000.00,NULL),('00200001',2,'2008-01-07','0001','003',4000.00,NULL),('00200001',3,'2008-01-09','0001','004',2000.00,NULL),('00200001',4,'2008-01-11','0001','003',1000.00,NULL),('00200001',5,'2008-01-13','0001','003',2000.00,NULL),('00200001',6,'2008-01-15','0001','004',4000.00,NULL),('00200001',7,'2008-01-19','0001','003',2000.00,NULL),('00200001',8,'2008-01-21','0001','004',3000.00,NULL),('00200001',9,'2008-01-23','0001','003',7000.00,NULL),('00200001',10,'2008-01-27','0001','004',1000.00,NULL),('00200001',11,'2008-01-30','0001','004',3000.00,NULL),('00200001',12,'2008-02-04','0001','003',2000.00,NULL),('00200001',13,'2008-02-08','0001','004',4000.00,NULL),('00200001',14,'2008-02-13','0001','003',2000.00,NULL),('00200001',15,'2008-02-19','0001','004',1000.00,NULL),('00200002',1,'2008-01-09','0001','001',3800.00,NULL),('00200002',2,'2008-01-20','0001','003',4200.00,NULL),('00200002',3,'2008-03-06','0001','004',1200.00,NULL),('00200003',1,'2008-01-11','0001','001',2500.00,NULL),('00200003',2,'2008-01-17','0001','003',1500.00,NULL),('00200003',3,'2008-01-20','0001','004',500.00,NULL),('00200003',4,'2008-02-09','0001','004',500.00,NULL),('00200003',5,'2008-02-25','0001','003',3500.00,NULL),('00200003',6,'2008-03-11','0001','004',500.00,NULL);

/*Table structure for table `parametro` */

DROP TABLE IF EXISTS `parametro`;

CREATE TABLE `parametro` (
  `chr_paracodigo` char(3) NOT NULL,
  `vch_paradescripcion` varchar(50) NOT NULL,
  `vch_paravalor` varchar(70) NOT NULL,
  `vch_paraestado` varchar(15) NOT NULL default 'ACTIVO',
  PRIMARY KEY  (`chr_paracodigo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `parametro` */

insert  into `parametro`(`chr_paracodigo`,`vch_paradescripcion`,`vch_paravalor`,`vch_paraestado`) values ('001','ITF - Impuesto a la Transacciones Financieras','0.08','ACTIVO'),('002','Número de Operaciones Sin Costo','15','ACTIVO');

/*Table structure for table `sucursal` */

DROP TABLE IF EXISTS `sucursal`;

CREATE TABLE `sucursal` (
  `chr_sucucodigo` char(3) NOT NULL,
  `vch_sucunombre` varchar(50) NOT NULL,
  `vch_sucuciudad` varchar(30) NOT NULL,
  `vch_sucudireccion` varchar(50) default NULL,
  `int_sucucontcuenta` int(11) NOT NULL,
  `vch_sucuestado` varchar(1) NOT NULL default '1',
  PRIMARY KEY  (`chr_sucucodigo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `sucursal` */

insert  into `sucursal`(`chr_sucucodigo`,`vch_sucunombre`,`vch_sucuciudad`,`vch_sucudireccion`,`int_sucucontcuenta`,`vch_sucuestado`) values ('001','Sipan','Chiclayo','Av. Balta 1456',2,'1'),('002','Chan Chan','Trujillo','Jr. Independencia 456',3,'1'),('003','Los Olivos','Lima','Av. Central 1234',0,'1'),('004','Pardo','Lima','Av. Pardo 345 - Miraflores',0,'1'),('005','Misti','Arequipa','Bolivar 546',0,'1'),('006','Machupicchu','Cusco','Calle El Sol 534',0,'1'),('007','Grau','Piura','Av. Grau 1528',0,'1'),('000','enrique','lima','av.santarosa',8,'0'),('008','los choches','lima','av.los dominicos',3,'0'),('009','jorge','lima','av.santarosa',2,'0');

/*Table structure for table `tipomovimiento` */

DROP TABLE IF EXISTS `tipomovimiento`;

CREATE TABLE `tipomovimiento` (
  `chr_tipocodigo` char(3) NOT NULL,
  `vch_tipodescripcion` varchar(40) NOT NULL,
  `vch_tipoaccion` varchar(10) NOT NULL,
  `vch_tipoestado` varchar(15) NOT NULL default 'ACTIVO',
  PRIMARY KEY  (`chr_tipocodigo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `tipomovimiento` */

insert  into `tipomovimiento`(`chr_tipocodigo`,`vch_tipodescripcion`,`vch_tipoaccion`,`vch_tipoestado`) values ('001','Apertura de Cuenta','INGRESO','ACTIVO'),('002','Cancelar Cuenta','SALIDA','ACTIVO'),('003','Deposito','INGRESO','ACTIVO'),('004','Retiro','SALIDA','ACTIVO'),('005','Interes','INGRESO','ACTIVO'),('006','Mantenimiento','SALIDA','ACTIVO'),('007','ITF','SALIDA','ACTIVO'),('008','Transferencia','INGRESO','ACTIVO'),('009','Transferencia','SALIDA','ACTIVO'),('010','Cargo por Movimiento','SALIDA','ACTIVO');

/*Table structure for table `visitantes2` */

DROP TABLE IF EXISTS `visitantes2`;

CREATE TABLE `visitantes2` (
  `iD` int(11) NOT NULL auto_increment,
  `iP` char(15) NOT NULL,
  `fechahora` datetime NOT NULL,
  `chr_emplcodigo` char(4) NOT NULL,
  PRIMARY KEY  (`iD`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `visitantes2` */

insert  into `visitantes2`(`iD`,`iP`,`fechahora`,`chr_emplcodigo`) values (14,'192.168.1.23','2003-08-30 10:47:56','0003'),(13,'192.168.1.10','2003-08-30 10:47:20','0006'),(12,'192.168.1.6','2003-08-30 10:46:46','0001'),(11,'192.168.1.24','2003-08-30 10:46:37','0004'),(10,'127.0.0.1','2003-08-30 10:46:27','0002'),(9,'192.168.1.26','2003-08-30 10:46:21','0008'),(15,'127.0.0.1','2011-12-02 08:20:17','0001');

/*Table structure for table `vconsolidado` */

DROP TABLE IF EXISTS `vconsolidado`;

/*!50001 DROP VIEW IF EXISTS `vconsolidado` */;
/*!50001 DROP TABLE IF EXISTS `vconsolidado` */;

/*!50001 CREATE TABLE  `vconsolidado`(
 `chr_cuencodigo` char(8) ,
 `vch_cliepaterno` varchar(25) ,
 `vch_clienombre` varchar(30) ,
 `dec_cuensaldo` decimal(12,2) ,
 `vch_monedescripcion` varchar(20) ,
 `dtt_movifecha` date ,
 `vch_tipodescripcion` varchar(40) ,
 `dec_moviimporte` decimal(12,2) 
)*/;

/*View structure for view vconsolidado */

/*!50001 DROP TABLE IF EXISTS `vconsolidado` */;
/*!50001 DROP VIEW IF EXISTS `vconsolidado` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vconsolidado` AS select `cuenta`.`chr_cuencodigo` AS `chr_cuencodigo`,`cliente`.`vch_cliepaterno` AS `vch_cliepaterno`,`cliente`.`vch_clienombre` AS `vch_clienombre`,`cuenta`.`dec_cuensaldo` AS `dec_cuensaldo`,`moneda`.`vch_monedescripcion` AS `vch_monedescripcion`,`movimiento`.`dtt_movifecha` AS `dtt_movifecha`,`tipomovimiento`.`vch_tipodescripcion` AS `vch_tipodescripcion`,`movimiento`.`dec_moviimporte` AS `dec_moviimporte` from ((((`cuenta` join `cliente` on((`cuenta`.`chr_cliecodigo` = `cliente`.`chr_cliecodigo`))) join `moneda` on((`cuenta`.`chr_monecodigo` = `moneda`.`chr_monecodigo`))) join `movimiento` on((`movimiento`.`chr_cuencodigo` = `cuenta`.`chr_cuencodigo`))) join `tipomovimiento` on((`movimiento`.`chr_tipocodigo` = `tipomovimiento`.`chr_tipocodigo`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
